#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <lora_client/lora_client.h>
#include <zephyr/kernel.h>
#include <string.h>

int main(void)
{
	printk("Welcome to lora_client example applications\n");
	printk("Wait 5 seconds...\n");
	k_msleep(5000);
	
	lora_client_auto_init();
	lora_client_set_client_id(0xFACE);
	
	//Configure master or slave depending on the switch
	static const struct gpio_dt_spec button1 = GPIO_DT_SPEC_GET(DT_ALIAS(sw2), gpios);
	if (!device_is_ready(button1.port)) {
		return 0;
	}
	gpio_pin_configure_dt(&button1, GPIO_INPUT);
	bool slave = gpio_pin_get_dt(&button1);

	lora_client_config(slave);
	
	if(slave){
		printk("create payload\n");
		char msg[10];
		strcpy(msg,"helloworld");

		printk("loop\n");
		while (1) {
			printk("send\n");
			lora_client_send(msg, sizeof(msg));
			k_msleep(10000);
		}
	}else{
		printk("start listening\n");
		lora_client_listen(30000);
		struct lora_client_message msg;
		printk("loop\n");
		while (1) {
			printk("read\n");
			lora_client_read(&msg);
			k_msleep(10000);
		}
	}
	
	return 0;
	
}
