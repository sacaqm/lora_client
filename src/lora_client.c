#include <zephyr/drivers/lora.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <string.h>
#include <lora_client/lora_client.h>
#include <stdlib.h>

const struct device * lora_client_dev;
uint16_t lora_client_client_id=0;
uint16_t lora_client_seq_id=0;
uint32_t lora_client_fifo_size=100;
struct lora_client_fifo lora_client_buffer;

void lora_client_handler(const struct device *dev, uint8_t *data, uint16_t len, int16_t rssi, int8_t snr){
	if(len==0)return;
	ARG_UNUSED(dev);
	ARG_UNUSED(rssi);
	ARG_UNUSED(snr);
	//printk("Received data: dev: %s, len: %d, RSSI: %d dBm, SNR:%d dBm, data:%s\n", dev->name, len, rssi, snr, data);
	struct lora_client_message msg;
	//printk("init message\n");
	//lora_client_init_message(&msg);
	//printk("mem copy\n");
	/*
	for(uint32_t i=0;i<sizeof(msg);i++){
		((uint8_t*)&msg)[i]=data[i];
	}
	*/
	lora_client_parse_message(&msg,data);
	//lora_client_print_message(&msg);
	//printk("sanity checks\n");
	if(!lora_client_check_crc(&msg)){return;}
	//printk("put in fifo\n");
	if(lora_client_add_buffer(&msg)){
		lora_client_print_message(&msg);
	}
}

void lora_client_auto_init(){
	lora_client_dev = DEVICE_DT_GET(DT_ALIAS(lora0));
	lora_client_init(lora_client_dev);
}

void lora_client_init(const struct device * dev){
	if (!device_is_ready(lora_client_dev)) {
		printk("LORA_CLIENT: %s Device not ready", lora_client_dev->name);
		return;
	}	
	lora_client_init_buffer();
}

void lora_client_config(bool slave){
	printk("LORA_CLIENT: configure mode=%s\n",(!slave?"read":"write"));
	struct lora_modem_config config;
	config.frequency = 865100000;
	config.bandwidth = BW_125_KHZ;
	config.datarate = SF_8;
	config.preamble_len = 0;//8
	config.coding_rate = CR_4_5;
	config.iq_inverted = false;
	config.public_network = false;
	config.tx_power = 14;
	config.tx = slave;

	if(lora_config(lora_client_dev, &config)< 0) {
		printk("Lora configuration failed... program termination\n");
		return;
	}
	printk("LORA_CLIENT: Lora configuration done\n");
}

void lora_client_set_client_id(uint16_t client_id){
	lora_client_client_id=client_id;
}

void lora_client_send(uint8_t *data, uint16_t data_len){
	printk("LORA_CLIENT: create payload\n");
	
	struct lora_client_message msg;
	msg.client_id=lora_client_client_id;
	msg.seq_id=lora_client_seq_id;
	msg.length=data_len;
	for(uint32_t i=0;i<data_len;i++){msg.payload[i]=data[i];}
	msg.crc=lora_client_calc_crc((uint8_t*)&msg,sizeof(msg)-1);
	lora_client_print_message(&msg);
	
	printk("LORA_CLIENT: send\n");
	if(lora_send(lora_client_dev, (char *)&msg, sizeof(msg))<0) { 
		printk("LORA_CLIENT: Lora send failed\n");
	}else{
		printk("LORA_CLIENT: Data sent\n");
		lora_client_seq_id++;
	}
}

void lora_client_set_max_size(uint32_t max_size){
	lora_client_fifo_size=max_size;
}

void lora_client_bind(){
	printk("LORA_CLIENT: Enable asynchronous reception\n");
	lora_recv_async(lora_client_dev, lora_client_handler);
	printk("LORA_CLIENT: async enabled\n");
}

void lora_client_listen(int64_t millis){
	printk("LORA_CLIENT: Listen\n");
	uint8_t data[255];
	int16_t rssi;
	int8_t snr;
	int32_t len;
	int64_t t0=k_uptime_get();
	while(true){
		if((k_uptime_get()-t0)>millis){break;}
		//len=lora_recv(lora_client_dev, data, 255, K_NO_WAIT, &rssi, &snr);
		//len=lora_recv(lora_client_dev, data, 255, K_USEC(100), &rssi, &snr);
		len=lora_recv(lora_client_dev, data, 255, K_MSEC(500), &rssi, &snr);
		//len=lora_recv(lora_client_dev, data, 255, K_SECONDS(1), &rssi, &snr);
		//len=lora_recv(lora_client_dev, data, 255, K_FOREVER, &rssi, &snr);
		printk("len:%d",len);
		if(len<=0) continue;
		printk("len: %d, data: 0x",len);for(uint32_t i=0;i<255; i++){printk("%02x",data[i]);}printk("\n");
		struct lora_client_message tmsg;
		lora_client_parse_message(&tmsg,data);
		//lora_client_print_message(&tmsg);
		if(!lora_client_check_crc(&tmsg)){continue;}
		if(lora_client_add_buffer(&tmsg)){
			lora_client_print_message(&tmsg);
		}
		break;
	}
	printk("LORA_CLIENT: Listen end\n");
}

bool lora_client_read(struct lora_client_message * msg){

	printk("LORA_CLIENT: Read\n");
	return lora_client_read_buffer(msg);
}

bool lora_client_check_crc(struct lora_client_message * data){
	uint8_t crc=lora_client_calc_crc((uint8_t*)data,sizeof(*data)-1);
	//printk("calc crc: %x, data crc: %x\n", crc, data->crc);
	return (data->crc==crc);
}
	
uint8_t lora_client_calc_crc(const uint8_t * data, uint8_t len){
  uint8_t crc = 0xFF;
  for (uint32_t current_byte = 0; current_byte < len; ++current_byte) {
      crc ^= (data[current_byte]);
      for (uint8_t crc_bit = 8; crc_bit > 0; --crc_bit) {
          if(crc & 0x80)
						crc = (crc << 1) ^ 0x31;
          else         
						crc = (crc << 1);
      }
  }
  return crc;
}

void lora_client_init_message(struct lora_client_message * msg){
	msg->client_id=0;
	msg->seq_id=0;
	msg->length=0;
	msg->crc=0;
	for(uint32_t i=0;i<LORA_CLIENT_PAYLOAD_SIZE;i++){msg->payload[i]=0;}
}

void lora_client_copy_message(struct lora_client_message * copy, struct lora_client_message * msg){
	copy->client_id=msg->client_id;
	copy->seq_id=msg->seq_id;
	copy->length=msg->length;
	copy->crc=msg->crc;
	for(uint32_t i=0;i<LORA_CLIENT_PAYLOAD_SIZE;i++){copy->payload[i]=msg->payload[i];}
}

void lora_client_parse_message(struct lora_client_message * msg, uint8_t *bytes){
	for(uint32_t i=0;i<sizeof(*msg);i++){
		((uint8_t*)msg)[i]=bytes[i];
	}
}

void lora_client_print_message(struct lora_client_message * msg){
	printk("id: %04x, sid: %3i, len: %3i, crc: %02x, data: ",msg->client_id,msg->seq_id, msg->length, msg->crc);
	for(uint32_t i=0;i<msg->length; i++){printk("%02x",msg->payload[i]);}
	printk("\n");
}

void lora_client_init_buffer(){
	lora_client_buffer.head=0;
	lora_client_buffer.tail=0;
	for(uint32_t i=0;i<LORA_CLIENT_FIFO_SIZE;i++){
		lora_client_init_message(&lora_client_buffer.data[i]);
	}		
}

void lora_client_print_buffer(){
	printk("LORA_CLIENT: print buffer\n");
	printk("head: %2i, tail: %2i\n", lora_client_buffer.head, lora_client_buffer.tail);
	for(uint32_t i=lora_client_buffer.head;i<lora_client_buffer.tail;i++){
		lora_client_print_message(&lora_client_buffer.data[i]);
	}		
}

bool lora_client_add_buffer(struct lora_client_message * msg){
	for(size_t i=lora_client_buffer.head;i<lora_client_buffer.tail;i++){
		if(lora_client_buffer.data[i].crc==msg->crc){return false;}
	}
	lora_client_copy_message(&lora_client_buffer.data[lora_client_buffer.tail],msg);
	lora_client_buffer.tail++;
	if(lora_client_buffer.tail>LORA_CLIENT_FIFO_SIZE){lora_client_buffer.tail=0;}
	return true;
}

bool lora_client_read_buffer(struct lora_client_message * msg){
	if(lora_client_buffer.head==lora_client_buffer.tail){
		lora_client_init_message(msg);
		return false;
	}
	lora_client_copy_message(msg,&lora_client_buffer.data[lora_client_buffer.head]);
	lora_client_buffer.head++;
	if(lora_client_buffer.head>LORA_CLIENT_FIFO_SIZE){lora_client_buffer.head=0;}
	return true;
}


