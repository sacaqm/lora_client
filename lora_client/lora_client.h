#ifndef LORACLIENT_H
#define LORACLIENT_H

/** 
 * LORA client for the Nordic nRF9160DK
 * based on the LORA_PHY and LORAWAN libraries
 *
 * https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/zephyr/connectivity/lora_lorawan/index.html
 *
 * Carlos.Solans_AT_CERN
 * May 2023
 */

#include <stdint.h>
#include <zephyr/device.h>

#define LORA_CLIENT_PAYLOAD_SIZE 200
#define LORA_CLIENT_FIFO_SIZE 200

struct lora_client_message{
	uint16_t client_id;
	uint16_t seq_id;	
	uint8_t length;
	uint8_t payload[LORA_CLIENT_PAYLOAD_SIZE];
	uint8_t crc;
};

struct lora_client_fifo{
	size_t head;
	size_t tail;
	struct lora_client_message data[LORA_CLIENT_FIFO_SIZE];
};

void lora_client_auto_init();

void lora_client_init(const struct device * dev);

void lora_client_set_max_size(uint32_t max_size);

void lora_client_config(bool slave);

void lora_client_set_client_id(uint16_t client_id);

void lora_client_send(uint8_t *data, uint16_t data_len);

bool lora_client_read(struct lora_client_message * data);

void lora_client_data_available();

void lora_client_bind();

void lora_client_listen(int64_t millis);

uint8_t lora_client_calc_crc(const uint8_t * data, uint8_t len);

bool lora_client_check_crc(struct lora_client_message * data);

void lora_client_init_message(struct lora_client_message * msg);

void lora_client_print_message(struct lora_client_message * msg);

void lora_client_parse_message(struct lora_client_message * msg, uint8_t *bytes);

void lora_client_copy_message(struct lora_client_message * copy, struct lora_client_message * msg);

void lora_client_init_buffer();

void lora_client_print_buffer();

bool lora_client_add_buffer(struct lora_client_message * msg);

bool lora_client_read_buffer(struct lora_client_message * msg);

void lora_client_handler(const struct device *dev, uint8_t *data, uint16_t len, int16_t rssi, int8_t snr);

#endif